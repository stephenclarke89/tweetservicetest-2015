package app.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import app.models.Tweet;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class TweetServiceAPI
{
  private MyTweetServiceProxy service;
  private String service_url = "http://localhost:9000"; //localhost:9000
  
  public TweetServiceAPI()
  {
    Gson gson = new GsonBuilder().create();

    Retrofit retrofit = new Retrofit.Builder().baseUrl(service_url)
                             .addConverterFactory(GsonConverterFactory
                             .create(gson))
                             .build();
    service = retrofit.create(MyTweetServiceProxy.class);
  }

  public Tweet createTweet(String id, Tweet newTweet) throws Exception
  {
    Call<Tweet> call = (Call<Tweet>) service.createTweet(id, newTweet);
    Response<Tweet> returnedTweet = call.execute();
    return returnedTweet.body();
  }

}