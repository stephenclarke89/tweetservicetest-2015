package app.main;

import java.util.List;

import app.models.Tweet;
import app.models.Tweeter;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface MyTweetServiceProxy
{
	@GET("/api/tweeters")
	Call<List<Tweeter>> getAllTweeters();
	
	@POST("/api/tweeters")
	Call<Tweeter> createTweeter(@Body Tweeter tweeter);
	
	@GET("/api/tweets")
	Call<List<Tweet>> getAllTweets();
	
	@POST("/api/tweeters/{id}/tweets")
	Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);
}
