package app.models;
import java.util.Date;
import java.util.UUID;

import com.google.common.base.Objects;

import static com.google.common.base.MoreObjects.toStringHelper;

public class Tweet
{

  public String id;
  public String messageTweet;
  public String datestamp;

  public Tweet(){}

  public Tweet(String messageTweet)
  {
    this.id       = UUID.randomUUID().toString();
    this.messageTweet    = messageTweet;
    this.datestamp  = new Date().toString();
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Tweet)
    {
      final Tweet other = (Tweet) obj;        
      return     Objects.equal(id,        other.id)
              && Objects.equal(messageTweet,   other.messageTweet)
              && Objects.equal(datestamp, other.datestamp)  ;                          
    }
    else
    {
      return false;
    }
  }  

  @Override
  public String toString()
  {
     return toStringHelper(this)
         .addValue(id)
         .addValue(messageTweet)
         .addValue(datestamp)
         .toString();

  }

}