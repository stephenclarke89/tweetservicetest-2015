package app.test;

import static org.junit.Assert.*;
import org.junit.Test;
import app.main.TweetServiceAPI;
import app.models.Tweet;


public class TweetTest
{
  private static TweetServiceAPI service = new TweetServiceAPI();

  @Test
  public void test() throws Exception
  {
    Tweet tweet = new Tweet("tweet_3");
    Tweet returnedTweet = service.createTweet(tweet.id, tweet);
    assertEquals(tweet, returnedTweet);
  }
}
